module Evergreen
  module Model
    module Navigation
      def self.included(base)
        base.acts_as_nested_set scope: 'type'
        base.extend FriendlyId

        base.friendly_id :navigation_label, use: :scoped, scope: [:type, :parent]

        base.validates_uniqueness_of :path, if: :using_custom_path
        base.validates_presence_of :path, if: :using_custom_path

        base.after_save :update_path!, :update_descendants_path
        base.after_move :update_path!

        def base.find_by_path(request_path)
          find_by! path: request_path
        end
      end

      def update_path!
        return path if using_custom_path

        update_column :path, generate_path
        path
      end

      def navigation_label
        self[:navigation_label].present? ? self[:navigation_label] : title
      end

      private

      def generate_path
        slug_ancestry.join('/')
      end

      def slug_ancestry
        self_and_ancestors.map(&:slug).reject(&:blank?)
      end

      def update_descendants_path
        reload.descendants.each(&:update_path!)
      end
    end

    module InheritedAttributes
      def inherited_attribute(*method_chain)
        value = method_chain.inject(self, :try)
        return value if value.present?
        upward_ancestors.detect { |ancestor| value = method_chain.inject(ancestor, :try) }
        value
      end

      def upward_ancestors
        ancestors.sort_by(&:level).reverse
      end
    end
  end
end
