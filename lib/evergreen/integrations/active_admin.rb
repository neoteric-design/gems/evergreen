module Evergreen
  module Integrations
    module ActiveAdmin
      def self.included(dsl)
        def dsl.sortable_tree
          config.paginate = false
          config.filters = false
          config.batch_actions = false
          config.sort_order = 'lft_asc'

          index do
            render 'evergreen/admin/sortable_tree'
          end

          collection_action :sort_tree, :method => :post do
            begin
              root_items  = params[param_key].select { |_, v| v == "null" }
              child_items = params[param_key].select { |_, v| v != "null" }

              [root_items, child_items].map { |items| sort_tree_items(items) }
              render json: {
                result: 'success',
                status: '200',
                message: I18n.t('evergreen.sort_tree.success', default: "Saved")
              }
            rescue
              render json: {
                result: 'error',
                status: '500',
                message: I18n.t('evergreen.sort_tree.error', default: "Error")
              }, status: :internal_server_error
            end
          end

          controller do
            helper AdminHelper
            helper TreeHelper

            def param_key
              active_admin_config.resource_name.param_key
            end
            helper_method :param_key

            private

            def sort_tree_items(items)
              previous_item = nil

              resource_class.transaction do
                items.each do |item_id, parent_id|
                  current_item = resource_class.find(item_id)

                  make_child_or_root(current_item, parent_id)
                  move_right_of_previous_item(current_item.reload,
                                            previous_item)

                  previous_item = current_item.reload
                end
              end
            end

            def make_child_or_root(item, parent_id)
              if "null" == parent_id
                item.move_to_root
              else
                parent = resource_class.find(parent_id)
                item.move_to_child_of(parent)
              end
            end

            def move_right_of_previous_item(item, previous_item)
              return if previous_item.nil?
              return if previous_item.parent != item.parent
              item.move_to_right_of(previous_item)
            end
          end
        end
      end
    end
  end
end


