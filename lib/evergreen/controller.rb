module Evergreen
  module Controller
    def self.included(base)
      base.before_action(:redirect_home_to_root)
    end

    def home
      @page = page_class.home_page
    end

    def show
      @page = page_class.published.find_by_path(params[:path])
    end

    private

    def page_class
      self.class.name.gsub('Controller', '').singularize.constantize
    end

    def redirect_home_to_root
      redirect_to root_path if request.path == '/home'
    end
  end
end
