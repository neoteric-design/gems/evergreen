module Evergreen
  module ErrorHandling
    def render_not_found(not_found_page, exception: nil, **options)
      defaults = { layout: 'application', template: 'pages/show', formats: [:html], status: :not_found }
      @page = not_found_page
      render defaults.merge(options)
    end

    def render_server_error(server_error_page, exception: nil, **options)
      defaults = { layout: 'application', template: 'pages/show', formats: [:html], status: :internal_server_error }
      log_error(exception)
      report_error(exception)
      @exception = exception
      @page = server_error_page
      render defaults.merge(options)
    end

    def log_error(e)
      logger.error e.message
      logger.error e.backtrace.join("\n")
    end

    def report_error(e)
      Raygun.track_exception(e, request.env) if defined? Raygun
    end
  end
end
