module Evergreen
  module SpecHelpers
    module Lint
      def self.included(base)
        def valid_attributes
          {
            title: "Title",
            body: "<html>",
            navigation_label:   "stub-num-#{subject.class.count + 1}"
          }
        end

        def new_instance(attributes = {})
          subject.class.new(valid_attributes.merge(attributes))
        end

        def create_instance(attributes = {})
          subject.class.create!((valid_attributes.merge(attributes)))
        end

        base.describe "attributes" do
          let(:page) { new_instance }
          specify do
            expect(page).to respond_to(:title, :body, :lft, :rgt, :parent_id,
                                       :hidden?, :drafted?, :meta_description,
                                       :navigation_label)
          end
        end

        base.describe "pathing and routing" do
          let!(:parent) { create_instance(navigation_label: 'parent') }
          let!(:page) { create_instance(navigation_label: 'page', parent: parent) }
          let!(:child) { create_instance(navigation_label: 'child', parent: page) }

          describe ".path" do
            it "is based on the navigation label" do
              expect(parent.path).to eq("parent")
            end

            it "supports a tree structure" do
              expect(page.path).to eq "parent/page"
              expect(child.path).to eq "parent/page/child"
            end

            it 'updates descendants when slugs change' do
              page.navigation_label = 'shortened title'

              page.save

              expect(page.reload.path).to eq "parent/shortened-title"
              expect(child.reload.path).to eq "parent/shortened-title/child"
            end

            it 'updates after a move' do
              expect(child.path).to eq('parent/page/child')
              child.move_to_child_of(parent)

              expect(child.reload.path).to eq 'parent/child'
            end

            context 'using_custom_path is not true' do
              it 'reverts a custom path to generated' do
                page.path = "wowowow"
                page.using_custom_path = nil

                page.save

                expect(page.path).to eq(page.send(:generate_path))
              end
            end

            context 'using_custom_path is true' do
              before(:each) { page.using_custom_path = true }

              it 'skips path generation' do
                page.path = 'my-silly-event'

                page.save

                expect(page.path).to eq('my-silly-event')
              end

              it 'validates uniqueness of path' do
                page.path = parent.path

                page.save

                expect(page).to_not be_valid
              end

              it 'validates presence of path' do
                page.path = ''; page.save

                expect(page).to_not be_valid

                page.path = nil; page.save

                expect(page).to_not be_valid
              end
            end
          end
        end

        base.describe "scopes" do
          let!(:visible) { create_instance(state: 'published', hidden: false) }
          let!(:drafted) { create_instance(state: 'drafted') }
          let!(:hidden) { create_instance(hidden: true) }
          let!(:hidden_published){ create_instance(hidden: true, state: 'published') }

          describe "::visible" do
            it "includes only published and not-hidden pages" do
              expect(subject.class.visible).to include(visible)
            end

            it "excludes hidden pages" do
              expect(subject.class.visible).to_not include(hidden)
              expect(subject.class.visible).to_not include(hidden_published)
            end

            it "excludes drafted pages" do
              expect(subject.class.visible).to_not include(drafted)
            end
          end
        end

        base.describe "inherited attributes" do
          let!(:root) { create_instance(title: 'Root') }
          let!(:parent) { create_instance(title: 'Mid Parent', parent: root) }
          let!(:child) { create_instance(title: 'Child', parent: parent) }

          describe "#upward_ancestors" do
            it "returns the ancestry from immediate parent to root" do
              grandchild = create_instance(title: 'Grandchild', parent: child)
              expect(grandchild.upward_ancestors).to eq([child, parent, root])
            end
          end

          it "can find an attribute present on self" do
            child.update meta_description: 'hi'
            root.update meta_description: 'bye'
            expect(child.inherited_attribute(:meta_description)).to eq('hi')
          end

          it "can find an attribute not present until immediate parent" do
            parent.update meta_description: 'hi'
            root.update meta_description: 'bye'

            expect(child.inherited_attribute(:meta_description)).to eq('hi')
          end

          it "can find an attribute not present until root" do
            root.update meta_description: 'bye'

            expect(child.inherited_attribute(:meta_description)).to eq('bye')
          end

          it "returns nil if never found in ancestry" do
            expect(child.inherited_attribute(:meta_description)).to eq(nil)
          end
        end

        base.describe 'System Pages' do
          it 'defines a method to systematically access a page' do
            subject.class.register_system_page 'events', "Calendar"

            expect(subject.class).to respond_to :events_page
          end

          it 'returns the page by title if exists' do
            subject.class.register_system_page 'events', "Calendar"
            page = create_instance(title: "Calendar")

            expect(subject.class.events_page).to eq(page)
          end

          it 'it should always return a page, even if not persisted' do
            subject.class.register_system_page 'events', "Calendar"
            subject.class.destroy_all
            expect(subject.class.events_page).to be_a(subject.class)
          end
        end

        base.describe 'navigation label' do
          let(:page) { new_instance(title: 'hi', navigation_label: 'bye') }
          context 'is set' do
            it 'is used' do
              expect(page.navigation_label).to eq('bye')
            end
          end

          context 'is not set' do
            it 'falls back to title' do
              page.navigation_label = nil
              expect(page.navigation_label).to eq('hi')
            end
          end
        end

        base.describe 'slug' do
          let(:nav_label) { "Mighty Oak" }
          let(:page) { new_instance(navigation_label: nav_label) }

          it 'is based on navigation_label' do
            page.save!
            expect(page.slug).to eq(nav_label.parameterize)
          end
        end
      end
    end
  end
end
