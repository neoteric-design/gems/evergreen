module Evergreen
  class Engine < ::Rails::Engine
    isolate_namespace Evergreen

    initializer 'evergreen.action_controller',
                :before => :load_config_initializers do
      ActiveSupport.on_load :action_controller do
        ::ActionController::Base.send(:helper, Evergreen::ViewHelper)
      end
    end
  end
end
