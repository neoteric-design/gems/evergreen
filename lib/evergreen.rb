require 'awesome_nested_set'
require 'font-awesome-rails'
require 'friendly_id'
require 'pubdraft'

require 'evergreen/controller'
require 'evergreen/error_handling'
require 'evergreen/model'

require "evergreen/integrations/active_admin"
require 'evergreen/spec_helpers'

require 'evergreen/engine'

module Evergreen
  def self.params
    [ :title, :navigation_label, :path, :using_custom_path, :body, :parent_id, :state, :hidden, :meta_description ]
  end
end
