module Evergreen
  class InstallGenerator < Rails::Generators::Base
    source_root File.expand_path("../templates", __FILE__)
    argument :model_name, type: :string, default: 'Page'

    def copy_templates
      template "home.html.erb", "app/views/#{@model_name.underscore.pluralize}/home.html.erb"
    end

    def generate_initial_resource
      generate 'evergreen:resource', @model_name
    end

    def setup_home_route
      route "root to: '#{@model_name.underscore.pluralize}#home'"
    end

    def add_rescues
      rescues = <<~RESCUES
        include Evergreen::ErrorHandling

        unless Rails.env.development?
          rescue_from 'StandardError' do |exception|
            render_server_error(#{@model_name}.server_error_page, exception: exception)
          end
          rescue_from 'ActionView::MissingTemplate',
                      'ActionController::UnknownFormat',
                      'ActiveRecord::RecordNotFound' do
            render_not_found(#{@model_name}.not_found_page)
          end
        end
        RESCUES

      inject_into_file 'app/controllers/application_controller.rb', rescues,
                        before: %r(^end$)
    end

    def install_migrations
      rake "evergreen:install:migrations"
    end

    def generate_admin
      generate 'evergreen:active_admin' if defined?(ActiveAdmin)
    end
  end
end
