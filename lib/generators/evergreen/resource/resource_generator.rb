module Evergreen
  class ResourceGenerator < Rails::Generators::Base
    source_root File.expand_path("../templates", __FILE__)
    argument :model_name, type: :string, default: 'Page'

    def copy_templates
      template "model.rb.erb", "app/model/#{model_name.underscore}.rb"
      template "model_spec.rb.erb", "spec/models/#{model_name.underscore}_spec.rb"
      template "controller.rb.erb", "app/controllers/#{model_name.pluralize.underscore}_controller.rb"
      template "show.html.erb", "app/views/#{model_name.pluralize.underscore}/show.html.erb"
      if defined?(ActiveAdmin)
        template "admin_resource.rb.erb", "app/admin/#{model_name.underscore}.rb"
      end
    end

    def configure_routes
      route "get '*path' => '#{model_name.underscore.dasherize.pluralize}#show', :as => :#{model_name.underscore}"
    end
  end
end
