namespace :evergreen do
  desc "Regenerate cached_path field"
  task regenerate_paths: :environment do
    Evergreen::Page.find_each(&:update_path!)
  end
end
