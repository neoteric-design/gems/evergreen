class AddStateToEvergreenPages < ActiveRecord::Migration[5.1]
  def up
    add_column :evergreen_pages, :state, :string, default: 'published'

    return unless column_exists?(:evergreen_pages, :drafted)

    Evergreen::Page.reset_column_information
    Evergreen::Page.find_each do |page|
      state = page[:drafted] ? 'drafted' : 'published'
      page.update_column :state, state
    end

    remove_column :evergreen_pages, :drafted
  end

  def down
    add_column :evergreen_pages, :drafted, :boolean, default: false, null: false

    Evergreen::Page.reset_column_information
    Evergreen::Page.find_each do |page|
      page.update_column :drafted, true if page.state == 'drafted'
    end

    remove_column :evergreen_pages, :state
  end
end
