class AddCustomPathSupport < ActiveRecord::Migration[5.1]
  def change
    rename_column :evergreen_pages, :cached_path, :path
    add_column :evergreen_pages, :using_custom_path, :boolean,  default: false
  end
end
