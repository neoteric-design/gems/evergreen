class AddNavigationLabelToEvergreenPages < ActiveRecord::Migration[5.1]
  def change
    add_column :evergreen_pages, :navigation_label, :string
  end
end
