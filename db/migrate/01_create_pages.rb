class CreatePages < ActiveRecord::Migration[5.1]
  def up
    create_table :evergreen_pages do |t|
      t.string  :title
      t.text    :body
      t.text    :meta_description
      t.string  :type,      :index => true
      t.string  :slug,      :index => true
      t.integer :lft,       :index => true
      t.integer :rgt,       :index => true
      t.integer :parent_id, :index => true
      t.boolean :hidden,    :index => true, default: false, null: false
      t.timestamps
    end
  end

  def down
    drop_table :evergreen_pages
  end
end
