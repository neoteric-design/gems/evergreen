class AddCachedPathToEvergreenPages < ActiveRecord::Migration[5.1]
  def change
    add_column :evergreen_pages, :cached_path, :string, index: true
  end
end
