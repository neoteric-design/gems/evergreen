$(function() {
  $togglePathEditable = function() {
    $('input.custom-path').prop('disabled', !$('input.using-custom-path').prop('checked'));
  };

  $togglePathEditable();
  $(document).change('input.using-custom-path', $togglePathEditable);
});
