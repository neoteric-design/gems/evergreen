//= require jquery.mjs.nestedSortable

updateSortableTreeStatus = function(statusZone, status, message) {
  statusZone.html(message).removeClass().addClass('tree-status ' + status)
  return statusZone;
};

$(document).on('click', '.sortable-tree .toggle', function() {
  $(this).closest('li').toggleClass('collapsed');
});

$(document).on('click', '.tree-controls .expand-all', function(e) {
  e.preventDefault();
  var $tree = $(this).closest('.sortable-tree');
  $tree.find('li').each(function() {
    if ($(this).children().length) {
      $(this).removeClass('collapsed');
    }
  });
});

$(document).on('click', '.tree-controls .collapse-all', function(e) {
  e.preventDefault();
  var $tree = $(this).closest('.sortable-tree');
  $tree.find('li').each(function() {
    if ($(this).children().length) {
      $(this).addClass('collapsed');
    }
  });
});

$(function() {
  $('.sortable-tree').each(function() {
    statusZone = $(this).find('.tree-status');
    tree = $(this).find('ol.roots');

    statusZone.hide();

    tree.nestedSortable({
      items: 'li',
      listType: 'ol',
      toleranceElement: '> div.sortable-item-wrapper',
      maxLevels: 5,
      opacity: 0.6,
      forcePlaceholderSize: true,
      update: function() {
        updateSortableTreeStatus(statusZone, 'pending', "Saving...")
        .fadeIn('fast');

        tree.nestedSortable('disable');
        var serialized = tree.nestedSortable('serialize');
        $.post($(this).data('url'), serialized)
        .done(function(data) {
          updateSortableTreeStatus(statusZone, data.result, data.message)
          .fadeOut(2000);
        })
        .fail(function(data) {
          updateSortableTreeStatus(statusZone, data.responseJSON.result,
                                   data.responseJSON.message)
        })
        .always(function() {
          tree.nestedSortable('enable');
        });
      }
    });
  });
});
