module Evergreen
  class Page < ActiveRecord::Base
    include Model::Navigation
    include Model::InheritedAttributes
    extend Pubdraft

    self.table_name = "evergreen_pages"

    pubdraft

    default_scope -> { order :lft }
    scope :hidden, -> { where(hidden: true) }
    scope :visible, -> { published.where.not(hidden: true) }

    def self.register_system_page(identifier, title)
      define_singleton_method("#{identifier}_page") { where(title: title).first_or_initialize }
    end

    def should_generate_new_friendly_id?
      title_changed? || navigation_label_changed?
    end

    def to_param
      path
    end
  end
end

