module Evergreen
  module ViewHelper
    def home?
      action_name == "home"
    end

    def browser_title(default, prepend = nil)
      default = strip_tags(default).html_safe

      return default unless prepend.present?
      strip_tags("#{prepend} - #{default}").html_safe
    end
  end
end
