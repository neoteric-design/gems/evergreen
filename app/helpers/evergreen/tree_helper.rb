module Evergreen
  module TreeHelper
    def sortable_tree_item(id, options = {})
      tag(:li, options.merge(id: "#{param_key}_#{id}"))
    end

    def sortable_tree_list(objects, options = {}, &block)
      return '' if objects.size == 0

      ol_tag = tag(:ol, options.fetch(:ol_options, {}))
      li_options = options.fetch(:li_options, {})
      path = [nil]

      output = tag(:ol, options.delete(:root_options))
      output << sortable_tree_item(objects.first.id, li_options)

      objects.each_with_index do |object, idx|
        if object.parent_id != path.last
          # We are on a new level, did we descend or ascend?
          if path.include?(object.parent_id)
            # Remove the wrong trailing path elements
            while path.last != object.parent_id
              path.pop
              output.safe_concat '</li></ol>'
            end
            output.safe_concat '</li>'
            output << sortable_tree_item(object.id, li_options)
          else
            path << object.parent_id
            output << ol_tag
            output << sortable_tree_item(object.id, li_options)
          end
        elsif idx != 0
          output.safe_concat '</li>'
          output << sortable_tree_item(object.id, li_options)
        end
        output << capture(object, path.size - 1, &block)
      end

      output.safe_concat '</li></ol>' * path.length
    end
  end
end
