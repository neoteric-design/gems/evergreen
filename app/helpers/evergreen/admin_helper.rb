module Evergreen
  module AdminHelper
    def hidden_label(item)
      return unless item.hidden
      state_label('Hidden')
    end

    def tree_options_for_select(item, klass = nil)
      klass ||= item.class
      nested_set_options(klass, item) { |i| "#{'-' * i.level} #{i.title}" }
    end
  end
end
