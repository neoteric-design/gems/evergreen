class ApplicationController < ActionController::Base
  include Evergreen::ErrorHandling

  unless Rails.env.development?
    rescue_from 'StandardError' do |exception|
      render_server_error(Page.server_error_page, exception: exception)
    end

    rescue_from 'ActionView::MissingTemplate', 'ActiveRecord::RecordNotFound' do
      render_not_found(Page.not_found_page)
    end
  end

  protect_from_forgery
end
