class SecondaryPagesController < ApplicationController
  include Evergreen::Controller

  def show
    @page = page_class.find_by_path params[:path]
    render 'pages/show'
  end
end
