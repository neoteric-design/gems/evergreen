class Page < Evergreen::Page
  register_system_page 'home', "Home Page"
  register_system_page 'not_found', "Page Not Found"
  register_system_page 'server_error', "Internal Server Error"
end
