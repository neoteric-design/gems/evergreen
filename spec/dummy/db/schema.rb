# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 5) do

  create_table "evergreen_pages", force: :cascade do |t|
    t.string   "title"
    t.text     "body"
    t.text     "meta_description"
    t.string   "type"
    t.string   "slug"
    t.integer  "lft"
    t.integer  "rgt"
    t.integer  "parent_id"
    t.boolean  "hidden",            default: false,       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "path"
    t.string   "navigation_label"
    t.boolean  "using_custom_path", default: false
    t.string   "state",             default: "published"
    t.index ["hidden"], name: "index_evergreen_pages_on_hidden"
    t.index ["lft"], name: "index_evergreen_pages_on_lft"
    t.index ["parent_id"], name: "index_evergreen_pages_on_parent_id"
    t.index ["rgt"], name: "index_evergreen_pages_on_rgt"
    t.index ["slug"], name: "index_evergreen_pages_on_slug"
    t.index ["type"], name: "index_evergreen_pages_on_type"
  end

end
