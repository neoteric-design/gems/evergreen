Rails.application.routes.draw do
  root :to => 'pages#home'
  get 'secondary(/*path)' => 'secondary_pages#show', as: :secondary_page
  get '*path' => 'pages#show', as: :page
end
