require 'rubygems'
require 'rails'
require 'active_record'
require 'action_controller'
require 'database_cleaner'
require 'friendly_id'
require 'awesome_nested_set'
require 'ostruct'

db = begin
       YAML.load(File.open('config/database.yml'))['test']
     rescue
       { :adapter => 'sqlite3',
         :database => 'db/evergreen.sqlite' }
     end
ActiveRecord::Base.establish_connection(db)

RSpec.configure do |config|
  config.before(:suite) do
    DatabaseCleaner.strategy = :transaction
    DatabaseCleaner.clean_with(:truncation)
  end

  config.before(:each) do
    DatabaseCleaner.start
  end

  config.after(:each) do
    DatabaseCleaner.clean
  end
end
