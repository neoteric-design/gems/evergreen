require 'spec_helper'

module Evergreen
  class Engine < ::Rails::Engine
    isolate_namespace Evergreen
  end

  Engine.routes.draw do
    root :to => 'pages#home'
    get 'secondary(/*path)' => 'secondary_pages#show', as: :secondary_page
    get '*path' => 'pages#show', as: :page
  end
end

RSpec.describe PagesController, type: :routing do
  let(:page) { Page.create(title: 'page') }

  it do
    expect(get: polymorphic_url(page, only_path: true)).to route_to(controller: 'pages', action: 'show', path: 'page')
  end
end

RSpec.describe SecondaryPagesController, type: :routing do
  let(:page) { SecondaryPage.create(title: 'secondary page') }

  it do
    expect(get: polymorphic_url(page, only_path: true)).to route_to(controller: 'secondary_pages', action: 'show', path: 'secondary-page')
  end
end