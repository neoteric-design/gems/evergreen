require 'spec_helper'

describe PagesController, type: :controller do
  describe "GET home" do
    it "finds the home page by slug 'home'" do
      page = Page.home_page
      page.save
      get :home
      expect(assigns(:page)).to eq(page)
    end

    it "redirects to root if /home is requested" do
      get :show, params: { path: 'home' }
      expect(response).to redirect_to(root_path)
    end
  end

  describe "GET not_found" do
    context "Page Not Found record exists" do
      it "renders PNF page and returns 404" do
        page = Page.not_found_page
        page.save
        get :show, params: { path: 'nonexistant-page' }

        expect(assigns(:page)).to eq(page)
        expect(response).to render_template(:show)
        expect(response).to have_http_status(:not_found)
      end
    end
    context "no Page Not Found page" do
      it "renders PNF page and returns 404" do
        Page.destroy_all
        get :show, params: { path: 'nonexistant-page' }

        expect(assigns(:page).title).to eq("Page Not Found")
        expect(response).to render_template(:show)
        expect(response).to have_http_status(:not_found)
      end
    end
  end

  describe "GET show" do
    let!(:root) { Page.create(title: "Root") }
    let!(:parent) { Page.create(title: "Parent", parent_id: root.id) }
    let!(:child) { Page.create(title: "Child", parent_id: parent.id) }

    it "finds a root page" do
      get :show, params: { path: 'root' }
      expect(assigns(:page)).to eq(root)
      expect(response).to have_http_status(:ok)
    end

    it "finds a child page" do
      get :show, params: { path: 'root/parent/child' }
      expect(assigns(:page)).to eq(child)
      expect(response).to have_http_status(:ok)
    end

    it "hands off to not_found if drafted" do
      child.draft!
      get :show, params: { path: 'root/parent/child' }
      expect(response).to have_http_status(:not_found)
    end
  end
end

describe SecondaryPagesController, type: :controller do
  describe "GET home" do
    let!(:home) { p = SecondaryPage.home_page; p.save; p }

    it 'finds specified home page' do
      get :show, params: { path: "secondary" }
      expect(assigns(:page)).to eq(home)
    end
  end

  describe "GET show" do
    let!(:root) { SecondaryPage.create(title: "Root") }
    let!(:child) { SecondaryPage.create(title: "Child", parent_id: root.id) }

    it "finds a root page" do
      get :show, params: { path: 'root' }
      expect(assigns(:page)).to eq(root)
      expect(response).to have_http_status(:ok)
    end

    it "finds a child page" do
      get :show, params: { path: 'root/child' }
      expect(assigns(:page)).to eq(child)
      expect(response).to have_http_status(:ok)
    end
  end
end
