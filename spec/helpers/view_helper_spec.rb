require 'ostruct'
require 'action_view'
require './app/helpers/evergreen/view_helper'

class ImplementingClass < ActionView::Base
  include Evergreen::ViewHelper
end

describe Evergreen::ViewHelper, type: :helper do
  let(:helper) { ImplementingClass.new }

  describe "#home?" do
    it "returns true when the action is home" do
      allow(helper).to receive(:action_name) { 'home' }
      expect(helper.home?).to be_truthy
    end

    it "returns false when the action is something else" do
      allow(helper).to receive(:action_name) { 'show' }
      expect(helper.home?).to be_falsey
    end
  end

  describe "#browser_title" do
    it "takes a default title" do
      expect(helper.browser_title('default')).to eq 'default'
    end

    it "takes a prepending title" do
      result = helper.browser_title('default', 'prepend')
      expect(result).to eq 'prepend - default'
    end

    it "strips html tags" do
      result = helper.browser_title("Nic <em>'Aitch'</em> Haynes")
      expect(result).to eq "Nic 'Aitch' Haynes"
    end
  end
end
