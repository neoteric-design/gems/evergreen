ENV["RAILS_ENV"] = "test"
require File.expand_path("../dummy/config/environment.rb",  __FILE__)

require 'evergreen'
require 'rspec/rails'
require 'database_cleaner'
require 'rails-controller-testing'

ENGINE_RAILS_ROOT=File.join(File.dirname(__FILE__), '../')
Dir[File.join(ENGINE_RAILS_ROOT, "spec/support/**/*.rb")].each {|f| require f }

RSpec.configure do |config|
  config.include Rails.application.routes.url_helpers  # url_for
  config.include Evergreen::Engine.routes.url_helpers
  config.use_transactional_fixtures = false

  config.before(:each) do
    DatabaseCleaner.start
  end

  config.after(:each) do
    DatabaseCleaner.clean
  end

  RSpec.configure do |config|
    config.include Rails::Controller::Testing::TestProcess, type: :controller
    config.include Rails::Controller::Testing::Integration, type: :controller
    config.include Rails::Controller::Testing::TemplateAssertions, type: :controller
  end
end

def it_requires(*attrs)
  attrs.each do |attr|
    it "requires #{attr}" do
      subject.valid?
      expect(subject.errors[attr]).to include("can't be blank")
    end
  end
end

