# Evergreen Changelog

5.0.7
=====

* Ruby 3 fixes

5.0.6
=====

* Scope friendly_id to ActiveRecord type as well as nested set parent by default
* Handle unknown formats as 404s by default

5.0.5
=====

* Error Handling: Pass request env to Raygun service in addition to raised
  exception. Fills out HTTP and request information for better diagnostics.

5.0.4
=====

* Fix confirmation on page delete

5.0.3
=====

* Fix sortable tree partial path refs

5.0.2
=====

* Reorganize generators for install, page resources, and activeadmin
* Namespace admin views

5.0.1
=====

* Fix: ID property on sortable tree
* Fix: Error messages on sort display properly again
* Code Health: Refactor sortable tree js
* Update: nestedSortable library

5.0.0.1
=======

* Fix: Default scope and active admin sort order respects tree order

5.0.0
=====

* Removed: Custom live saving, preview, and old 'admin actions' views and javascript. Replace with something that plays nice with universal AdminToolbox utilities
* Change: Break up JS/SCSS into one feature per file, for easier picking and choosing of features
* Change: Replace custom drafted/publish feature with pubdraft support
* Change: Remove auto-injection of stuff into ActionController and ActiveAdmin's ResourceDSL, in favor regular `include`s
* Change: Minimize footprint of ActiveAdmin customization hooks
* Code Health: Clean up the linter and make it friendlier to implementers
* Optimization: Reduce db churn on rendering page tree for admins

4.3.2
=====

* Fix previewable module referencing deprecated methods
