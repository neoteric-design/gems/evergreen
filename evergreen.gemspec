$:.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'evergreen/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'evergreen'
  s.version     = Evergreen::VERSION
  s.authors     = ['Joe Sak', 'Nic Haynes', 'Madeline Cowie']
  s.email       = ['joe@joesak.com', 'nic@nicinabox.com', 'madeline@cowie.me']
  s.homepage    = 'http://neotericdesign.com'
  s.summary     = 'Neoteric CMS Evergreen Pages'
  s.description = 'What the summary says'

  s.files = Dir['{app,config,db,lib,vendor}/**/*'] + ['Rakefile', 'README.md']
  s.test_files = Dir['test/**/*']

  s.add_runtime_dependency 'awesome_nested_set', '~> 3.0', '>= 3.0.1'
  s.add_runtime_dependency 'font-awesome-rails', '~> 4.0'
  s.add_runtime_dependency 'friendly_id', '~> 5.1'
  s.add_runtime_dependency 'rails', '>= 4.2.1'

  s.add_dependency 'pubdraft', '>= 1.0.0'

  s.add_development_dependency 'database_cleaner'
  s.add_development_dependency 'rails-controller-testing'
  s.add_development_dependency 'rspec-rails', '~> 3.0'
  s.add_development_dependency 'sqlite3'
end
